﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameView : MonoBehaviour {

    private Game game;
    
    public void Initialize(Game game) {
        this.game = game;
    }
}