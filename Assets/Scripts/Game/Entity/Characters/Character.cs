﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "Characters/Character")]
public class Character : ScriptableObject {

    public List<Action> actions;
    public RuntimeAnimatorController animatorController;
    public int healthTotal = 100;

}