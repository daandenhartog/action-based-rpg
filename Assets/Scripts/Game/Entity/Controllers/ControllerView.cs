﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControllerView : ScriptableObject {

    protected Controller controller;
    protected Entity entity;

    protected List<RectTransform> selectedActions = new List<RectTransform>();
    protected Vector2 actionSize;

    private RectTransform selectedParent;
    private GameObject viewUI;
    private Vector2 selectedSize;
    private float offset = 10f;

    public virtual void Initialize(Controller controller, Entity entity, GameObject viewUI) {
        this.controller = controller;
        this.entity = entity;
        this.viewUI = viewUI;

        // Set the Action prefab size
        actionSize = ControllerViewShared.actionSize;

        // Get selected position and size
        selectedParent = viewUI.transform.Find("Selected").GetComponent<RectTransform>();
        selectedSize = selectedParent.sizeDelta;

        entity.onActionAdded += AddAction;
        entity.onActionRemoved += RemoveAction;
    }

    // Add the selected Action to the scene
    public virtual void AddAction(Action action, int index) {
        selectedActions.Add(GetAction(action));
    }

    // Remove the selected Action from the scene
    // Rename and move the Actions below it to their newly set values
    public virtual void RemoveAction(int index) {
        ControllerViewShared.SetActionToPooling(selectedActions[index]);
        selectedActions.RemoveAt(index);

        for (int i = index; i < selectedActions.Count; i++) {
            SetActionNames(selectedActions[i], "Selected", i);
            SetActionSelectedPosition(selectedActions[i], i);
        }
    }

    // Moves the RectTransform to the desired index
    // Moves all actions in the range of index - indexdesired to their correct position
    public void MoveAction(int index, int indexDesired) {
        RectTransform r = selectedActions[index];
        selectedActions.RemoveAt(index);
        selectedActions.Insert(indexDesired, r);

        for (int i = index < indexDesired ? index : indexDesired; i <= (index < indexDesired ? indexDesired : index); i++) {
            SetActionSelectedPosition(selectedActions[i], i);
        }
    }

    // Gets an Action from pooling
    // Set parent, name, position and colors
    protected virtual RectTransform GetAction(Action action) {
        RectTransform r = ControllerViewShared.GetAction();
        r.SetParent(selectedParent, false);
        SetActionNames(r, "Selected", selectedActions.Count);
        SetActionSelectedPosition(r, selectedActions.Count);
        SetActionColors(r, action.color);
        
        return r;
    }

    // Set the Unity Editor name and index of the Action
    protected void SetActionNames(RectTransform r, string editorName, int index) {
        r.name = string.Format("{0}{1:00}", editorName, index);
        r.GetComponentInChildren<Text>().text = string.Format("{0:00}", index);
    }

    // Set the Action at the correct position
    protected void SetActionSelectedPosition(RectTransform r, int index) {
        r.localPosition = new Vector2(0, GetPositionSelected(index));
    }

    // Set the correct color
    protected void SetActionColors(RectTransform r, Color color) {
        r.GetComponent<Image>().color = color;
    }

    // Calculate the position based on the index & offset
    protected float GetPositionSelected(int index) {
        float position = actionSize.y * index + actionSize.y / 2f;
        float offsetAmount = offset * index;
        
        return selectedSize.y / 2f - (position + offsetAmount);
    }
}