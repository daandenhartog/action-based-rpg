﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public static class ControllerViewShared {

    public static Vector2 actionSize { get { return actionPrefab.GetComponent<RectTransform>().sizeDelta; } }

    private static List<RectTransform> actions = new List<RectTransform>();
    private static GameObject actionPrefab;

    static ControllerViewShared() {
        actionPrefab = Resources.Load("Prefabs/Action") as GameObject;
    }

    public static void Reset() {
        actions = new List<RectTransform>();
    }

    public static void SetActionToPooling(RectTransform r) {
        Button b = r.GetComponent<Button>();
        b.interactable = true;
        b.onClick.RemoveAllListeners();

        r.gameObject.SetActive(false);
    }

    public static RectTransform GetAction() {
        foreach (RectTransform r in actions) {
            if (!r.gameObject.activeSelf) {
                r.gameObject.SetActive(true);
                return r;
            }
        }

        RectTransform t = GameObject.Instantiate(actionPrefab).GetComponent<RectTransform>();
        actions.Add(t);
        return t;
    }
}
