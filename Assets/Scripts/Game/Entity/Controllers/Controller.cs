﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Controller : ScriptableObject {
    
    protected Game game;
    protected Entity entity;
    protected ControllerView view;

    public virtual void Initialize(Game game, Entity entity, GameObject viewUI) {
        this.game = game;
        this.entity = entity;

        entity.opponent = game.GetOpponent(entity);
    }

    // Signals to add an Action
    public void AddAction(Action action) {
        entity.AddAction(action);
    }

    // Signals to remove an Action
    public void RemoveAction(Action action) { RemoveAction(entity.actions.IndexOf(action)); }
    public void RemoveAction(int index) {
        entity.RemoveAction(index);
    }

    // Move an Action to the desired index
    public void MoveAction(int index, int indexDesired) {
        // Prevents the current Action from being switched when it is in progress
        if ((index == 0 || indexDesired == 0) && entity.actionInProgress) return;

        entity.MoveAction(index, indexDesired);
        view.MoveAction(index, indexDesired);
    }

    public virtual void OnUpdate() {

    }
}