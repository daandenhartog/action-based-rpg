﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlayerView : ControllerView {
    
    private Canvas canvas;
    private RectTransform availableParent;
    private RectTransform draggedAction;
    private Vector2 availableSize;
    private int draggedIndex;
    private float offset = 20f;
    
    public override void Initialize(Controller controller, Entity entity, GameObject viewUI) {
        base.Initialize(controller, entity, viewUI);

        canvas = FindObjectOfType<Canvas>();
        // Get available position
        availableParent = GameObject.Find("AvailablePlayer").GetComponent<RectTransform>();
        // Instantiate all available actions to the player
        DisplayAvailableActions();

        entity.onCurrentActionInvoked += OnCurrentActionInvoke;
        entity.onCurrentActionFinished += OnCurrentActionFinished;
    }

    public void OnUpdate() {
        if (draggedAction) {
            OnDrag();
        }
    }

    // Remove a selected Action in the scene, within selectedParent
    // Override the base functionality by removing all events
    public override void RemoveAction(int index) {
        EventTrigger trigger = selectedActions[index].GetComponent<EventTrigger>();
        trigger.triggers.RemoveRange(0, trigger.triggers.Count);

        base.RemoveAction(index);
    }

    // Initializes for dragging Actions
    public void OnBeginDrag(RectTransform r) {
        draggedAction = r;
        draggedIndex = selectedActions.IndexOf(draggedAction);

        r.GetComponent<Button>().interactable = false;
        draggedAction.SetAsLastSibling();
    }

    // Resets for dragging Actions
    public void OnEndDrag() {
        SetActionSelectedPosition(draggedAction, draggedIndex);
        draggedAction.GetComponent<Button>().interactable = true;
        draggedAction.SetSiblingIndex(draggedIndex);
        draggedAction = null;
    }

    public void OnCurrentActionInvoke() {
        selectedActions[0].GetComponent<Button>().interactable = false;
    }

    public void OnCurrentActionFinished() {
        selectedActions[0].GetComponent<Button>().interactable = true;
    }

    // Dragging Actions logic
    private void OnDrag() {
        Vector2 mouse;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.transform as RectTransform, Input.mousePosition, canvas.worldCamera, out mouse);
        
        Vector2 current = new Vector2(0, draggedAction.localPosition.y);
        Vector2 desired = new Vector2(0, mouse.y);

        // Clamp the desired position within the boundaries of the selectedParent
        desired = new Vector2(0, Mathf.Clamp(desired.y, GetPositionSelected(selectedActions.Count - 1), GetPositionSelected(entity.CanMoveAction(0) ? 0 : 1)));

        // Checks whether the dragged Action has passed another Action
        // And if so, switches the two Actions
        if(draggedIndex > 0 && draggedAction.localPosition.y >= GetPositionSelected(draggedIndex - 1) - (actionSize.y / 2f)) {
            controller.MoveAction(draggedIndex, draggedIndex - 1);
            draggedIndex = draggedIndex - 1;
        }

        if (draggedIndex < selectedActions.Count - 1 && draggedAction.localPosition.y <= GetPositionSelected(draggedIndex + 1) + (actionSize.y / 2f)) {
            controller.MoveAction(draggedIndex, draggedIndex + 1);
            draggedIndex = draggedIndex + 1;
        }

        // Move towards desired position
        draggedAction.localPosition = Vector2.Lerp(current, desired, 10f * Time.deltaTime);
    }

    // Override the base functionality by adding an event which cancels the action when clicked
    // OnBeginDrag and OnEndDrag events are also added to support moving Actions
    protected override RectTransform GetAction(Action action) {
        RectTransform r = base.GetAction(action);
        r.GetComponent<Button>().onClick.AddListener(() => controller.RemoveAction(action));

        EventTrigger trigger = r.GetComponent<EventTrigger>();
        EventTrigger.Entry onBeginDrag = new EventTrigger.Entry();
        onBeginDrag.eventID = EventTriggerType.BeginDrag;
        onBeginDrag.callback.AddListener((eventData) => { OnBeginDrag(r); });
        trigger.triggers.Add(onBeginDrag);
        EventTrigger.Entry onEndDrag = new EventTrigger.Entry();
        onEndDrag.eventID = EventTriggerType.EndDrag;
        onEndDrag.callback.AddListener((eventData) => { OnEndDrag(); });
        trigger.triggers.Add(onEndDrag);

        return r;
    }

    // Size the availableParent so all Actions will fit in there
    // Display all Actions in a row
    // When an Action is clicked, the Controller's OnAddAction will be called
    private void DisplayAvailableActions() {
        int count = entity.character.actions.Count;
        availableSize = new Vector2(count * actionSize.x + (count - 1) * offset, availableParent.sizeDelta.y);
        availableParent.sizeDelta = availableSize;

        for (int i = 0; i < count; i++) {
            RectTransform r = ControllerViewShared.GetAction();
            r.SetParent(availableParent, false);
            SetActionNames(r, "Available", i);
            SetActionAvailablePosition(r, i);
            SetActionColors(r, entity.character.actions[i].color);

            int index = i;
            r.GetComponent<Button>().onClick.AddListener(() => controller.AddAction(entity.character.actions[index]));
        }
    }

    // Set the Action at the correct position
    private void SetActionAvailablePosition(RectTransform r, int index) {
        r.localPosition = new Vector2(GetPositionAvailable(index), 0);
    }

    // Calculate the position based on the index & offset
    private float GetPositionAvailable(int index) {
        float position = actionSize.x * index + actionSize.x / 2f;
        float offsetAmount = offset * index;

        return -(availableSize.x / 2f) + (position + offsetAmount);
    }
}