﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player", menuName = "Controllers/Player")]
public class Player : Controller {

    private KeyCode[] numericHotkeys = new KeyCode[] { KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9, };
    private KeyCode moveHotkey = KeyCode.RightShift;
    private KeyCode cancelHotkey = KeyCode.LeftShift;
    private KeyCode cancelCurrentHotkey = KeyCode.Space;
    private PreviousHotkey previousMoveHotkey;
    private float timeBetweenHotkeyPresses = 4f;

    public override void Initialize(Game game, Entity entity, GameObject viewUI) {
        view = CreateInstance<PlayerView>();
        view.Initialize(this, entity, viewUI);

        base.Initialize(game, entity, viewUI);
    }

    public override void OnUpdate() {
        base.OnUpdate();
        (view as PlayerView).OnUpdate();

        DetectHotkeyInput();
    }

    private void DetectHotkeyInput() {
        // Loop through all available numeric keycodes, and detect if any of them have been pressed
        for (int i = 0; i < numericHotkeys.Length; i++) {
            if (Input.GetKeyDown(numericHotkeys[i])) {
                // If the moveHotkey is pressed, it will see if it has been previously pressed
                // If it has, the time should be close enough, the keycodes can't be the same and both indexes should be smaller than the current amount of Actions
                // If it hasn't been pressed, a new instance will be created with the current input and time
                if (Input.GetKey(moveHotkey)) {
                    if (previousMoveHotkey != null && previousMoveHotkey.time >= Time.deltaTime - timeBetweenHotkeyPresses && previousMoveHotkey.keycode != numericHotkeys[i] && Mathf.Max(System.Array.IndexOf(numericHotkeys, previousMoveHotkey.keycode), i) < entity.actions.Count) {
                        MoveAction(System.Array.IndexOf(numericHotkeys, previousMoveHotkey.keycode), i);
                        previousMoveHotkey = null;
                    } else {
                        previousMoveHotkey = new PreviousHotkey(numericHotkeys[i], Time.deltaTime);
                    }
                    break;
                }

                // If the cancelHotkey is pressed, it will cancel the Action if it exists
                if (Input.GetKey(cancelHotkey)) {
                    if (i < entity.actions.Count) {
                        RemoveAction(i);
                    }
                    break;
                }

                // Since none of the other hotkeys have been used, an Action will be added
                if (i < entity.character.actions.Count) {
                    AddAction(entity.character.actions[i]);
                }
            }
        }

        // Check to see if the hotkey to cancel the current action has been called
        if (Input.GetKeyDown(cancelCurrentHotkey)) {
            RemoveAction(0);
        }
    }

    private class PreviousHotkey {

        public KeyCode keycode;
        public float time;

        public PreviousHotkey(KeyCode keycode, float time) {
            this.keycode = keycode;
            this.time = time;
        }
    }
}