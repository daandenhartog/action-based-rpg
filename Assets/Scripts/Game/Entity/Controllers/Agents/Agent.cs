﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Agent", menuName = "Controllers/Agent")]
public class Agent : Controller {

    public RangedFloat interactionTime = new RangedFloat(0.5f, 1.3f);
    public bool canPlay = true;

    private int maximumAmountOfActions = 4;
    private float timer = 0f;

    public override void Initialize(Game game, Entity entity, GameObject viewUI) {
        view = CreateInstance<ControllerView>();
        view.Initialize(this, entity, viewUI);

        base.Initialize(game, entity, viewUI);

        timer = interactionTime.GetValue();
    }

    public override void OnUpdate() {
        base.OnUpdate();

        // Prevent from playing when entity has died or is not allowed to play
        if (!canPlay || entity.hasDied) return;

        timer -= Time.deltaTime;

        if(timer <= 0f) {
            // Select a random action from the list
            timer = interactionTime.GetValue();
            AddAction(entity.character.actions[Random.Range(0, entity.character.actions.Count)]);
        }
    }
}