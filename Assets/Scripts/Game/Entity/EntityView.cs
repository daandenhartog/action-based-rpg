﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EntityView : MonoBehaviour {
    
    private Entity entity;
    private Animator animator;
    private SpriteRenderer spriteRenderer;

    private RectTransform currentActionProgress;
    private RectTransform warmupCurrentActionProgress, durationCurrentActionProgress, cursorCurrentActionProgress;
    private Image amountHealth, animatedHealth;
    private Text timeUntilInvoke, timeUntilFinish;
    private Text entityName, entitySelectedActions;
    private Text textHealth;

    private Color colorBase = Color.white;

    private IEnumerator healthAmountCoroutine, healthAnimatedCoroutine;
    private IEnumerator spriteColorsCoroutine;

    public void Initalize(Entity entity, GameObject viewUI) {
        this.entity = entity;

        animator = GetComponentInChildren<Animator>();
        animator.runtimeAnimatorController = entity.character.animatorController;
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();

        entityName = viewUI.transform.Find("Name").GetComponent<Text>();
        entitySelectedActions = viewUI.transform.Find("Actions").GetComponent<Text>();
        Transform actionInformation = viewUI.transform.Find("ActionInformation");
        timeUntilInvoke = actionInformation.Find("Invoke").GetComponent<Text>();
        timeUntilFinish = actionInformation.Find("Finish").GetComponent<Text>();

        currentActionProgress = viewUI.transform.Find("ActionProgress").GetComponent<RectTransform>();
        warmupCurrentActionProgress = currentActionProgress.Find("Warmup").GetComponent<RectTransform>();
        durationCurrentActionProgress = currentActionProgress.Find("Duration").GetComponent<RectTransform>();
        cursorCurrentActionProgress = currentActionProgress.Find("Cursor").GetComponent<RectTransform>();

        RectTransform health = viewUI.transform.Find("Health").GetComponent<RectTransform>();
        health.sizeDelta = new Vector2(Mathf.Clamp(health.sizeDelta.x * (entity.character.healthTotal / 100f), health.sizeDelta.x, health.sizeDelta.x * 1.8f), health.sizeDelta.y);
        amountHealth = health.Find("Amount").GetComponent<Image>();
        animatedHealth = health.Find("Animated").GetComponent<Image>();
        textHealth = health.Find("Text").GetComponent<Text>();

        SetControllerName();

        entity.onCurrentActionChanged += SetCurrentActionProgress;
        entity.onActionsChanged += SetSelectedActions;
    }

    public void InvokeAction(Action action) {
        animator.SetTrigger(animator.ContainsParameter(action.animationTrigger) ? action.animationTrigger : "SetAttack");
    }

    public void ReceiveDamage(int healthCurrent, int healthTotal) {
        SetHealth(healthCurrent, healthTotal);

        if (spriteColorsCoroutine != null) StopCoroutine(spriteColorsCoroutine);
        spriteColorsCoroutine = LerpSpriteColors(spriteRenderer, 0f, 0.3f, Color.red, colorBase);
        StartCoroutine(spriteColorsCoroutine);
    }

    public void SetCurrentActionProgress() { SetCurrentActionProgress(entity.currentAction); }
    public void SetCurrentActionProgress(Action action) {
        if (action == null) { Debug.Log("Action is null.."); return; }

        warmupCurrentActionProgress.sizeDelta = new Vector2(currentActionProgress.sizeDelta.x * (action.warmupTime / action.totalTime), warmupCurrentActionProgress.sizeDelta.y);
        durationCurrentActionProgress.sizeDelta = new Vector2(currentActionProgress.sizeDelta.x * (action.durationTime / action.totalTime), durationCurrentActionProgress.sizeDelta.y);
    }

    public void UpdateTimeUntilInvoke(float time) {
        timeUntilInvoke.text = string.Format("Time until Invoke {0:00.00}", time);
    }

    public void UpdateTimeUntilFinish(float time) {
        timeUntilFinish.text = string.Format("Time until Finish {0:00.00}", time);
    }

    public void UpdateCurrentActionProgressCursor(float currentTime, float totalTime) {
        cursorCurrentActionProgress.localPosition = new Vector2(Mathf.Lerp(-currentActionProgress.sizeDelta.x / 2f, currentActionProgress.sizeDelta.x / 2f, currentTime / totalTime), cursorCurrentActionProgress.localPosition.y);
    }
    
    public void SetHealth(int healthCurrent, int healthTotal) {
        textHealth.text = string.Format("{0}/{1}", healthCurrent, healthTotal);

        if (healthAmountCoroutine != null) StopCoroutine(healthAmountCoroutine);
        healthAmountCoroutine = AnimateFilledImage(amountHealth, 0.0f, 0.2f, amountHealth.fillAmount, healthCurrent / (float)healthTotal);
        StartCoroutine(healthAmountCoroutine);

        if (healthAnimatedCoroutine != null) StopCoroutine(healthAnimatedCoroutine);
        healthAnimatedCoroutine = AnimateFilledImage(animatedHealth, 0.15f, 0.4f, animatedHealth.fillAmount, healthCurrent / (float)healthTotal);
        StartCoroutine(healthAnimatedCoroutine);
    }

    public void SetSelectedActions() {
        string text = "";
        for (int i = 0; i < entity.actions.Count; i++) {
            text += string.Format("{0}\n", entity.actions[i].name);
        }

        entitySelectedActions.text = text;
    }

    public void SetHasDied() {
        colorBase = Color.gray;
    }

    private void SetControllerName() {
        entityName.text = entity.controller.name;
    }

    private void SetAnimatorSpeed(float speed) {
        animator.speed = speed;
    }

    private IEnumerator LerpSpriteColors(SpriteRenderer renderer, float delay, float time, Color a, Color b) {
        float timer = 0f;
        yield return new WaitForSeconds(delay);
        while(timer < time) {
            timer += Time.deltaTime;
            renderer.color = Color.Lerp(a, b, timer / time);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator AnimateFilledImage(Image image, float delay, float time, float start, float end) {
        float timer = time;
        yield return new WaitForSeconds(delay);
        while (timer > 0f) {
            timer -= Time.deltaTime;
            image.fillAmount = Mathf.Lerp(end, start, timer / time);
            yield return new WaitForEndOfFrame();
        }
    }
}