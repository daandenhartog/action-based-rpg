﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : InteractableObject {

    public Controller controller;
    public Entity opponent;
    
    public System.Action onCurrentActionInvoked, onCurrentActionFinished;

    private EntityView view;
    private int maxActions = 4;    

    public float currentTime { get; set; }
    public bool actionInProgress { get; set; }
    public bool hasDied { get; set; }

    public override void Initialize(GameObject viewUI) {
        base.Initialize(viewUI);

        view = GetComponent<EntityView>();
        view.Initalize(this, viewUI);
        view.SetHealth(healthCurrent, character.healthTotal);
        
        onCurrentActionChanged += ResetCurrentActionTime;
    }

    public void OnUpdate() {
        // Return if there is no current Action or the opponent has died
        if (!currentAction || opponent.hasDied) return;

        // Add to currentTime and update view
        currentTime += Time.deltaTime * timescale;
        view.UpdateTimeUntilInvoke(Mathf.Clamp(currentAction.warmupTime - currentTime, 0f, currentAction.warmupTime) * (1f / timescale));
        view.UpdateTimeUntilFinish(Mathf.Clamp(currentAction.warmupTime + currentAction.durationTime - currentTime, 0f, currentAction.warmupTime + currentAction.durationTime) * (1f / timescale));
        view.UpdateCurrentActionProgressCursor(currentTime, currentAction.totalTime);

        // Check to see if the current Action should be invoked
        if (currentTime >= currentAction.warmupTime && !actionInProgress) {
            InvokeCurrentAction();
        }

        // Check to see if the current Action should be finished
        if (currentTime >= currentAction.totalTime) {
            FinishCurrentAction();
        }
    }

    #region Public Action methods
    public override void AddAction(Action action) {
        // Prevents an Action from being added if the limit has been reached
        if (!CanAddAction()) return;

        base.AddAction(action);
    }

    public override void RemoveAction(int index) {
        // Prevents an Action from being removed if it's the only Action
        if (!CanRemoveAction(index)) return;

        base.RemoveAction(index);
    }

    public override void MoveAction(int indexCurrent, int indexDesired) {
        // Prevents an Action to be switched to current, while the current Action is in progress
        if (!CanMoveAction(indexDesired)) return;

        base.MoveAction(indexCurrent, indexDesired);
    }

    // Returns whether an Action can be added
    public bool CanAddAction() {
        return actions.Count < maxActions;
    }

    // Returns whether an Action can be removed
    // An Action can't be removed when it's the only one
    // An Action can't be removed when it is already in progress
    public bool CanRemoveAction(int index) {
        return !(actions.Count < 1 || (index == 0 && actionInProgress));
    }

    // Returns whether an Action can be moved
    // The current Action can only be moved if it's not in progress
    public bool CanMoveAction(int indexDesired) {
        return !(indexDesired == 0 && actionInProgress);
    }
    #endregion

    #region Current Action
    // Sets the correct values to invoke the current Action
    private void InvokeCurrentAction() {
        actionInProgress = true;
        currentAction.Invoke(opponent);
        view.InvokeAction(currentAction);

        onCurrentActionInvoked.InvokeSafe();
    }
    
    // Contains the loop logic for when the current Action is the only Action
    private void FinishCurrentAction() {
        currentTime -= currentAction.totalTime;
        actionInProgress = false;

        onCurrentActionFinished.InvokeSafe();

        if (actions.Count > 1) {
            RemoveAction(0);
        }
    }

    // Reset current Action time
    private void ResetCurrentActionTime() {
        currentTime = 0f;
    }
    #endregion

    public void ReceiveDamage(int damage) {
        healthCurrent = Mathf.Clamp(healthCurrent - damage, 0, healthCurrent);

        if (healthCurrent <= 0) {
            Die();
        }
        
        view.ReceiveDamage(healthCurrent, character.healthTotal);
    }

    private void Die() {
        timescale = 0f;
        hasDied = true;
        view.SetHasDied();

        for(int i = actions.Count - 1; i >= 0; i--) {
            RemoveAction(i);
            Debug.Log("Removing action " + i);
        }

        Debug.Log(string.Format("Entity <color=orange>{0}</color> has died", character.name));
    }
}