﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct EffectContainer {

    public enum Target { Target, Self };

    public Effect effect;
    public Target target;
    public int amount;
    public float duration;

    public void Invoke() {
        effect.Invoke();
    }
}
