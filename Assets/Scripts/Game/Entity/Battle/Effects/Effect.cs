﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// An instant effect
/// Is applied instantly
/// 
/// Examples:
/// Stun - Stun an enemy and stop their attack
/// </summary>
[CreateAssetMenu(fileName = "Effect", menuName = "Effects/Effect")]
public class Effect : ScriptableObject {

    public new string name;
    public Color color;
    
    public virtual void Invoke() {
        Debug.Log(string.Format("Calling {0}", GetDebug()));
    }

    protected virtual string GetDebug() {
        return string.Format("Effect <color={0}>{1}</color>", color, name);
    }
}