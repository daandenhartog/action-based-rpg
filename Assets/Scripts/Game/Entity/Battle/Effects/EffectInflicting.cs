﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Effect", menuName = "Effects/Effect Inflicting")]
public class EffectInflicting : Effect {

    private int amount;

    public virtual void Invoke(int amount) {
        this.amount = amount;

        Invoke();
    }

    protected override string GetDebug() {
        return string.Format("Continous Effect <color={0}>{1}</color> for {2} amount", color, name, amount);
    }
}
