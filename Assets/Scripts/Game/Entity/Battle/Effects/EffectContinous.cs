﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Effect", menuName = "Effects/Effect Continous")]
public class EffectContinous : Effect {

    private float duration;

    public virtual void Invoke(float duration) {
        this.duration = duration;

        Invoke();
    }

    protected override string GetDebug() {
        return string.Format("Continous Effect <color={0}>{1}</color> for {2} seconds", color, name, duration / 60f);
    }
}
