﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Action", menuName = "Actions/Action")]
public class Action : ScriptableObject {

    public List<EffectContainer> entries = new List<EffectContainer>();

    public Color color;
    public int damage = 10;
    public float warmupTime = 2f, durationTime = 0.5f;
    public string animationTrigger = "SetAttack";

    public float totalTime { get { return warmupTime + durationTime; } }

    public void Invoke(Entity target) {
        target.ReceiveDamage(damage);
    }

    public void Invoke() {
        foreach(EffectContainer e in entries) {
            if (e.effect == null) continue;

            e.Invoke();
        }
    }
}