﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System;

[CustomEditor(typeof(Action))]
public class ActionInspector : Editor {

    private ReorderableList list;
    private int[] heights;
    private int labelWidth = 80;
    private int margin = 2;

    private float lineHeight { get { return EditorGUIUtility.singleLineHeight; } }

    private void OnEnable() {
        list = new ReorderableList(serializedObject, serializedObject.FindProperty("entries"), true, true, true, true);
        list.drawHeaderCallback = (Rect rect) => {
            EditorGUI.LabelField(rect, "Effect entries");
        };

        SetLineHeights();

        list.drawElementCallback =
            (Rect rect, int index, bool isActive, bool isFocused) => {
                var element = list.serializedProperty.GetArrayElementAtIndex(index);

                // Draw Effect selection
                EditorGUI.PropertyField(
                    new Rect(rect.x, rect.y, rect.width / 2f, lineHeight),
                    element.FindPropertyRelative("effect"), GUIContent.none);

                //EditorGUI.LabelField(
                //    new Rect(rect.x + rect.width / 2f, rect.y, labelWidth, lineHeight),
                //    "Target");

                EditorGUI.PropertyField(
                    new Rect(rect.x + rect.width / 2f, rect.y, rect.width / 2f, lineHeight),
                    element.FindPropertyRelative("target"), GUIContent.none);

                // Get the Effect from EffectContainer
                Effect effect = element.FindPropertyRelative("effect").objectReferenceValue as Effect;

                EditorGUI.indentLevel += 2;

                if (effect as EffectContinous != null) {
                    EditorGUI.LabelField(
                        new Rect(rect.x, rect.y + lineHeight + margin, 200, lineHeight),
                        "Duration");
                    EditorGUI.PropertyField(
                        new Rect(rect.x + labelWidth, rect.y + lineHeight + margin, rect.width - labelWidth, EditorGUIUtility.singleLineHeight),
                        element.FindPropertyRelative("duration"), GUIContent.none);

                    heights[index] = 2;
                }

                if (effect as EffectInflicting != null) {
                    EditorGUI.LabelField(
                        new Rect(rect.x, rect.y + lineHeight + margin, 200, lineHeight),
                        "Amount");
                    EditorGUI.PropertyField(
                        new Rect(rect.x + labelWidth, rect.y + lineHeight + margin, rect.width - labelWidth, EditorGUIUtility.singleLineHeight),
                        element.FindPropertyRelative("amount"), GUIContent.none);

                    heights[index] = 2;
                }

                EditorGUI.indentLevel -= 2;
            };
        
        list.onChangedCallback = (list) => {
            SetLineHeights();
        };

        list.elementHeightCallback = (index) => {
            Repaint();
            return (EditorGUIUtility.singleLineHeight + margin) * heights[index];
        };
    }

    private void SetLineHeights() {
        heights = new int[serializedObject.FindProperty("entries").arraySize];
        for (int i = 0; i < heights.Length; i++) {
            heights[i] = 1;
        }
    }

    public override void OnInspectorGUI() {
        serializedObject.Update();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }
}