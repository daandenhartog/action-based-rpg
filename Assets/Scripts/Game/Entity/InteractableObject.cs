﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour {

    public Character character;
    [HideInInspector]
    public List<Action> actions = new List<Action>();
    public TEAM team;
    public float timescale = 1f;

    public Action currentAction { get { return actions == null || actions.Count == 0 ? null : actions[0]; } }

    public System.Action onCurrentActionChanged, onActionsChanged;
    public System.Action<Action, int> onActionAdded;
    public System.Action<int> onActionRemoved;

    protected int healthCurrent;

    public virtual void Initialize(GameObject viewUI) {
        healthCurrent = character.healthTotal;
    }

    #region Action interaction
    // Add an action to the list of actions
    public virtual void AddAction(Action action) {
        actions.Add(action);

        onActionAdded.InvokeSafe(action, actions.Count);
        onActionsChanged.InvokeSafe();
        if (actions.Count == 1) onCurrentActionChanged.InvokeSafe();
    }
    
    // Remove an action from the list of actions
    public void RemoveAction(Action action) { RemoveAction(actions.IndexOf(action)); }
    public virtual void RemoveAction(int index) {
        actions.RemoveAt(index);

        onActionRemoved.InvokeSafe(index);
        onActionsChanged.InvokeSafe();
        if (index == 0) onCurrentActionChanged.InvokeSafe();
    }
    
    // Move an action to a given position in the list of actions
    public virtual void MoveAction(int indexCurrent, int indexDesired) {
        Action a = actions[indexCurrent];
        actions.RemoveAt(indexCurrent);
        actions.Insert(indexDesired, a);

        onActionsChanged.InvokeSafe();
        if (indexCurrent == 0 || indexDesired == 0) onCurrentActionChanged.InvokeSafe();
    }
    #endregion
}