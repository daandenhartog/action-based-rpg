﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionEvent {

    public enum Move { ADD, REMOVE };

    public Move move;
    public float timestamp;

}