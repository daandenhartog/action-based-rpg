﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public enum TEAM { RED, BLUE };
public class Game : MonoBehaviour {

    public Entity[] entities;
    public GameView view;

    private List<Entity> entitiesRed = new List<Entity>(), entitiesBlue = new List<Entity>(); 
    private SceneSettings settings;
    private bool isPaused;

    private void Awake() {
        ControllerViewShared.Reset();

        settings = FindObjectOfType<SceneSettings>() as SceneSettings;
        view.Initialize(this);

        for (int i = 0; i < entities.Length; i++) {
            if (entities[i].gameObject.activeSelf) {
                entities[i].controller = Instantiate(settings.controllers[i]);
                entities[i].Initialize(GameObject.Find(string.Format("Entity{0:00}", i)));
                entities[i].controller.Initialize(this, entities[i], GameObject.Find(string.Format("Entity{0:00}", i)));

                if(entities[i].team == TEAM.RED) {
                    entitiesRed.Add(entities[i]);
                } else {
                    entitiesBlue.Add(entities[i]);
                }
            }
        }
    }

    public void SetSceneController(Controller controller) {
        settings.SetController(controller, 0);
    }

    public void ReloadScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void TogglePause(Text text) {
        isPaused = !isPaused;
        text.text = isPaused ? "Unpause" : "Pause";
    }

    // Temporary work around
    // Will be updated in Milestone 0.3, when multiple targets will be added
    public Entity GetOpponent(Entity entity) {
        foreach (Entity e in entities)
            if (e != entity)
                return e;

        return null;
    }

    private void Update() {
        foreach (Entity e in entities) {
            if (e.gameObject.activeSelf) {
                e.controller.OnUpdate();
                e.OnUpdate();
            }
        }
    }
}