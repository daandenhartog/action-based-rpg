﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneSettings : MonoBehaviour {

    public Controller[] controllers;

    private void Awake() {
        if (FindObjectsOfType<SceneSettings>().Length > 1) {
            DestroyImmediate(gameObject);
            return;
        }
        
        DontDestroyOnLoad(gameObject);
    }

    public void SetController(Controller controller, int index) {
        controllers[index] = controller;
    }
}