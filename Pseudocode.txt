Action Based RPG Pseudocode

Effect:
	Class met bepaalde values
	Kan een invoke method uitvoeren welke dingen toepast

	Bijvoorbeeld:
		Het Effect 'Damage' inflict damage
		Het Effect 'Status' inflict een status, zoals poison of slow
		Het Effect 'Heal' healt de target

InflictEffects:
	Heeft een Effect
	Heeft een target (Target | Self)

	Bijvoorbeeld:
		Effect Damage op Target Target
		Effect Heal op Target Self

Action:
	Collection van InflictEffects

	Bijvoorbeeld:
		Recoil - Een aanval welke grote schade op de tegenstander doet, 
				 maar ook schade op de speler zelf
		Effect Damage (10 - 12) op Target Target
		Effect Damage (3 - 4) op Target Self

Character:
	Collection van Actions
	Collection van Stats

Entity: 
	Heeft een Character
	Heeft een collection van current Stats

	Begint de huidige actions altijd met een 'lege' action, wanneer beide spelers de eerste action hebben toegevoegd begint de game

Player - EntityAI
	Heeft een entity
	Kiest bepaalde acties

Wat heb ik nog nodig?
	Hoe pauzeer ik de game?
	Hoe start ik de game?
	Hoe krijg ik de target?
	Hoe selecteer ik een character?

Iedere entity krijgt een eigen selected bar, deze word geplaatst dmv een vooraf aangegeven positie
Alleen de player (controller) krijgt een eigen available bar